const assert = require('assert')
const formula = require('./index2')

describe('formula test', () => {
    it('should be 8' , () => {
        assert.deepStrictEqual(formula.power(2,3), 8)
    })
    it('should be 25', () => {
        assert.deepStrictEqual(formula.square(5), 25)
    })
})