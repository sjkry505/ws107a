const http = require('http')

const server = http.createServer((req,res) => {
    switch (req.url) {
        case '/hello':
            res.writeHead(200, {'Content-Type': 'text/plain;charset=utf-8'})
            res.end('你好')
            break
        case '/name':
            res.writeHead(200, {'Content-Type': 'text/plain;charset=utf-8'})
            res.end('姓名:王岳駿')
            break
        case '/id':
            res.writeHead(200, {'Content-Type': 'text/plain;charset=utf-8'})
            res.end('學號:110510521')
        default:
            res.writeHead(404)
            res.end()
    }
})

server.listen(3000);