const koa = require('koa')
const app = module.exports = new koa()

app.use(async function (ctx) {
    switch (ctx.url) {
        case '/hello':
            ctx.status = 200
            ctx.body = '你好'
            break
        case '/name':
            ctx.status = 200
            ctx.body= '姓名:王岳駿'
            break
        case '/id':
            ctx.status = 200
            ctx.body= '學號:110510521'
            break;
        default:
            ctx.status = 404
    }
})

if (!module.parent) app.listen(3000);