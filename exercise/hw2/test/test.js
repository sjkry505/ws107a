const assert = require('assert')
const lodash = require('../index')

describe('chunk', function () {
    it("chunk(['a','b','c','d'],3) equalTo  [ [ 'a', 'b', 'c' ], [ 'd' ] ]", function () {
        assert.deepStrictEqual(lodash.chunk(['a','b','c','d'],3), [ [ 'a', 'b', 'c' ], [ 'd' ] ])
    })
    it("_.chunk(['a', 'b', 'c', 'd'], 3) notEqualTo [ [ 'a', 'b'], ['c' , 'd' ] ]", function () {
        assert.notDeepStrictEqual(lodash.chunk(['a', 'b', 'c', 'd'], 3), [ [ 'a', 'b' ], ['c', 'd'] ])
      })
    it("compact([0, 1, false, 2, '', 3, null, undefined, NaN]) equalTo [ 1, 2, 3 ]", function () {
        assert.deepStrictEqual(lodash.compact([0, 1, false, 2, '', 3, null, undefined, NaN]), [ 1, 2, 3 ])
    })
    it("compact([0, 1, false, 2, '', 3, null, undefined, NaN]) NotequalTo [ 0, 1, 2, 3 ]", function () {
        assert.notDeepStrictEqual(lodash.compact([0, 1, false, 2, '', 3, null, undefined, NaN]), [0, 1, 2, 3 ])
    })
    it("concat([1], 2, [3], [[4]]) equalTo [ 1, 2, 3, [ 4 ] ]", function () {
        assert.deepStrictEqual(lodash.concat([1], 2, [3], [[4]]), [ 1, 2, 3, [ 4 ] ])
    })
    it("concat([1], 2, [3], [[4]]) NotequalTo [ 1, 2, 3, 4 ]", function () {
        assert.notDeepStrictEqual(lodash.concat([1], 2, [3], [[4]]), [ 1, 2, 3, 4])
    })
    it("_.difference([2, 1, 4, 8], [2, 3, 8, 4], [3, 1, 4 , 9])  equalTo []", function () {
        assert.deepStrictEqual(lodash.difference([2, 1, 4, 8], [2, 3, 8, 4], [3, 1, 4 , 9]), [])
    })
    it("_.difference([2, 1, 4, 8], [2, 3, 8, 4], [3, 1, 4 , 9])  NotequalTo [2,1,4,8]", function () {
        assert.notDeepStrictEqual(lodash.difference([2, 1, 4, 8], [2, 3, 8, 4], [3, 1, 4 , 9]), [2,1,4,8])
    })
})