const V = require('./view')
const M = require('./model')
const logger = require('koa-logger')
const router = require('koa-router')()
const koaBody = require('koa-body')

const Koa = require('koa')
const app = (module.exports = new Koa())
let index = 0

app.use(logger())
app.use(koaBody())

router
  .get('/', list)
  .get('/post/new', add)
  .get('/post/:id', show)
  .post('/post', create)
  .get('/edit', edit)
  .get('/delete/:id', deletepost)
  .post('/modify', modify)

app.use(router.routes())

async function list (ctx) {
  const posts = M.list()
  ctx.body = await V.list(posts)
}

async function add (ctx) {
  ctx.body = await V.new()
}

async function show (ctx) {
  const id = ctx.params.id
  index = id
  const post = M.get(id)
  if (!post) ctx.throw(404, 'invalid post id')
  ctx.body = await V.show(post)
}

async function create (ctx) {
  const post = ctx.request.body
  M.add(post)
  ctx.redirect('/')
}

async function edit (ctx) {
  const post = M.get(index)
  if (!post) ctx.throw(404, 'invalid post id')
  ctx.body = await V.modify(post)
}

async function modify (ctx) {
  const post = ctx.request.body
  const targetpost = M.get(index)
  targetpost.body = post.body
  M.modify(targetpost)
  ctx.redirect('/')
}

async function deletepost (ctx) {
  const id = ctx.params.id
  const post = M.remove(id)

  if (!post) ctx.throw(404, 'Post not found')
  
  ctx.redirect('/')
}

if (!module.parent) {
  app.listen(3000)
  console.log('Server run at http://localhost:3000')
}
