function power(num, pow) {
    let x = 1
    for(let i=1; i<=pow; i++) {
        x = x*num
    }
    return x
}

function square(num) {
    let x = num*num
    return x
}

const formula = {
    power: power,
    square: square
}

module.exports = formula