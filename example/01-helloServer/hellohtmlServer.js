const http = require('http'); //與網站有關的模組，client與server端都可用

const port = 3000, hostname = 'localhost'

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end('<p>Hello World</p><a href="https://tw.youtube.com">youtube</a>\n');
});

server.listen(port, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

/* 等同於
server.listen(port, function () {
  console.log(`Server running at http://${hostname}:${port}/`);
});
*/