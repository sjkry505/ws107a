const M = module.exports = {}

const posts = []

M.add = function (post) {
  const id = posts.push(post) - 1
  post.created_at = new Date()
  post.id = id
}

M.get = function (id) {
  return posts[id]
}

M.list = function () {
  return posts
}

M.modify = function (post) {
  let NewPost = posts[post.id]
  post.created_at = NewPost.created_at
  posts[post.id] = NewPost
}
