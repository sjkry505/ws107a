const M = module.exports = {}

const posts = []
let index = 0

M.add = function (post) {
  const id = index
  post.created_at = new Date()
  post.id = id.toString()
  posts.push(post)
  index++
}

M.get = function (id) {
  for (let post of posts) {
    if (post.id === id) {
      return post
    }
  }
}

M.list = function () {
  return posts
}

M.modify = function (post) {
  for (let oldpost of posts) {
    if (oldpost.id === post.id) {
      oldpost = post
    }
  }
}

M.remove = function (id) {
  for (let i = 0; i < posts.length; i++) {
    if (posts[i].id === id) {
      posts.splice(i, 1)
      return true
    }
  }
}
