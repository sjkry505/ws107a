function chunk (array ,x) {
    const list = []
    for(let i=0; i<array.length; i+=x) {
        list.push(array.slice(i,i+x))
    }
    return list
}

function compact (array) {
    const list = []
    for(let i=0; i<array.length; i++) {
        if(!isNaN(array[i]) && typeof array[i] === 'number' && array[i] !== 0)    
            list.push(array[i])
    }
    return list
}

function concat() {
    const list = arguments[0]
    for(let i=1; i<arguments.length; i++) {
        if(Array.isArray(arguments[i])) {
            list.push(arguments[i].pop())
        }
        else list.push(arguments[i])
    }
    return list
}

function difference() {
    let list = arguments[0];
    for (let i =1 ; i<arguments.length; i++) {
        let x = arguments[i]
        for (let j = 0; j<list.length; j++) {
            for(let k=0; k<x.length; k++) {
                if(list[j] === x[k])
                    list[j] = NaN
            }
        }
    }
    return compact(list)
}

const lodashTest = {
    chunk : chunk,
    compact : compact,
    concat : concat,
    difference: difference,
}
module.exports = lodashTest